import { Component, WritableSignal, inject, signal } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ItemListService } from './item-list.service';
import { Item } from './item.interface';
import { ItemHeaderComponent } from "./item-header/item-header.component";
import { ItemBodyComponent } from "./item-body/item-body.component";

@Component({
    selector: 'his-item-list',
    standalone: true,
    templateUrl: './item-list.component.html',
    styleUrls: ['./item-list.component.scss'],
    imports: [CommonModule, ItemHeaderComponent, ItemBodyComponent]
})
export class ItemListComponent {
  public itemService: ItemListService = inject(ItemListService);
  public items: WritableSignal<Item[]> = signal([]);

  ngOnInit() {
    this.items.set(
      [
        {
          header: 'header1',
          body: 'body1'
        }
      ]
    )
  }
}
