import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Item } from '../item.interface';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'his-item-header',
  standalone: true,
  imports: [CommonModule, FormsModule],
  templateUrl: './item-header.component.html',
  styleUrls: ['./item-header.component.scss']
})
export class ItemHeaderComponent {
  @Input() item!: Item;
  @Output() insertItem = new EventEmitter<any>();
  @Output() deleteItem = new EventEmitter<any>();
  @Output() updateItem = new EventEmitter<any>();

  public originalItem!: Item;
  public isEdit = false;

  ngOnChanges(changes: any): void {
    if (changes.article) {
      this.originalItem = changes.item.currentValue;
      this.item = Object.assign({}, changes.item.currentValue);
    }
  };

  doInsertItem() {
    this.insertItem.emit(this.item);
  }

  doDeleteItem() {
    this.deleteItem.emit(this.item);
  }

  doUpdateTitle() {
    const newItem: Item = {
      header: this.item.header,
      body: this.item.body
    }
    this.updateItem.emit(newItem);
  };

  doCancelUpdateHeader() {
    this.item.header = this.item.header;
    this.isEdit = false;
  };
}
